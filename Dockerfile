FROM openjdk:8
COPY gradlew .
COPY gradle gradle
COPY build.gradle .
COPY settings.gradle .
COPY src src


FROM openjdk:8
COPY build/libs/*.jar api_tester.jar


EXPOSE 8080
ENTRYPOINT ["java", "-jar", "api_tester.jar"]